/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 24.977076838437558, "KoPercent": 75.02292316156245};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.02741610122868146, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.059322033898305086, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.0, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.01606425702811245, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.022727272727272728, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.0, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.0, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.0, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.007692307692307693, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.034274193548387094, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.048672566371681415, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.0, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.0, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.0, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.003787878787878788, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [0.015873015873015872, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [0.06439393939393939, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.0, 500, 1500, "portfolioByID"], "isController": false}, {"data": [0.0, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.0078125, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "getPastEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.0078125, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.0, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.0, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.8925619834710744, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.0, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [0.0, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 5453, 4091, 75.02292316156245, 37222.68292682928, 49, 224403, 9004.0, 118181.00000000003, 150935.2, 219072.50000000003, 13.582008881981235, 51.26244942239723, 24.267870130290643], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getUpcomingEvents", 118, 75, 63.559322033898304, 11012.889830508473, 145, 90616, 9003.0, 12152.900000000029, 28455.1, 90512.83, 0.30136328577922844, 0.16597028283455156, 0.4326211231401033], "isController": false}, {"data": ["findAllFeeGroup", 113, 74, 65.48672566371681, 35903.070796460204, 9002, 208949, 9099.0, 115592.6, 148015.19999999995, 208461.52, 0.29066701649599624, 0.19339786366173561, 0.3130915226514491], "isController": false}, {"data": ["findAllChildActiveSubsidies", 130, 103, 79.23076923076923, 39282.48461538462, 3130, 166138, 9004.0, 117142.20000000001, 131822.69999999998, 164310.86, 0.32381806406615854, 0.21182936189159568, 0.5277854969984557], "isController": false}, {"data": ["getListDomainByLevel", 108, 77, 71.29629629629629, 46185.19444444444, 2175, 209854, 9004.5, 123067.40000000001, 162782.19999999995, 209726.29, 0.26903684808385975, 0.510187688232378, 0.47843369175850453], "isController": false}, {"data": ["getPendingEvents", 129, 93, 72.09302325581395, 41724.86821705426, 120, 221906, 9005.0, 116780.0, 154625.5, 218652.19999999987, 0.3216364062492208, 0.16820315967252925, 0.41837860656636927], "isController": false}, {"data": ["getPortfolioTermByYear", 249, 171, 68.67469879518072, 21204.377510040136, 132, 136886, 9003.0, 75818.0, 109586.0, 125878.5, 0.6202438629485247, 0.4136394295437845, 0.7871051666749699], "isController": false}, {"data": ["findAllCurrentFeeTier", 131, 117, 89.31297709923665, 57993.893129771, 8076, 224403, 9005.0, 185116.8, 211833.59999999998, 223727.80000000002, 0.3263089722512828, 0.5414733293790166, 0.8259695860110596], "isController": false}, {"data": ["findAllClassResources", 110, 67, 60.90909090909091, 9816.04545454546, 132, 88018, 9003.0, 11780.9, 16758.099999999966, 84766.07000000002, 0.2829909571525964, 0.15666504000463077, 0.39298158307714076], "isController": false}, {"data": ["listBulkInvoiceRequest", 138, 109, 78.98550724637681, 47872.33333333334, 3147, 215235, 9003.0, 129215.1, 164749.49999999988, 214624.25999999998, 0.3446415727643251, 0.26213704544149086, 0.5220108196850276], "isController": false}, {"data": ["findAllCustomSubsidies", 112, 83, 74.10714285714286, 40990.28571428572, 1950, 209374, 9004.5, 118159.8, 123485.2, 208216.74000000005, 0.2879503080039902, 0.20417737642561112, 0.3599378850049877], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 133, 108, 81.203007518797, 31170.75187969926, 160, 176775, 9004.0, 114201.8, 119496.39999999998, 176308.18, 0.33140470744190453, 5.342406822015738, 0.47736517917657145], "isController": false}, {"data": ["getAllChildDiscounts", 133, 105, 78.94736842105263, 35472.03007518797, 142, 224081, 9506.0, 114163.0, 126742.89999999997, 223385.02, 0.3315674665004674, 0.1819905928638205, 0.54624444920536], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 109, 88, 80.73394495412845, 49190.8256880734, 9002, 216742, 9003.0, 157510.0, 205038.5, 216356.30000000002, 0.2715137263888924, 0.15431493926437218, 0.3560966157619946], "isController": false}, {"data": ["getMyDownloadPortfolio", 130, 89, 68.46153846153847, 16374.300000000003, 83, 122829, 9003.0, 23854.000000000007, 78844.54999999997, 120684.72999999998, 0.34456801772669926, 0.18943734610399593, 0.35365330725660243], "isController": false}, {"data": ["getListDomain", 248, 174, 70.16129032258064, 26802.362903225792, 49, 214743, 9003.0, 114474.4, 122637.29999999999, 201533.98999999987, 0.6182688472277623, 0.77491763297143, 1.0241124655651674], "isController": false}, {"data": ["getLessonPlan", 113, 71, 62.83185840707964, 11285.353982300883, 58, 118142, 9003.0, 13151.400000000014, 39360.49999999998, 111475.89999999997, 0.31767272400143937, 0.1831411586128666, 0.5093931765726205], "isController": false}, {"data": ["getAdvancePaymentReceipts", 112, 84, 75.0, 40221.33928571429, 1105, 139127, 9322.5, 116099.1, 119193.45, 137553.22000000006, 0.2795687651797103, 0.21670674547011234, 0.4458357358774091], "isController": false}, {"data": ["findAllProgramBillingUpload", 118, 86, 72.88135593220339, 42850.771186440696, 138, 217829, 9005.0, 117333.90000000001, 122271.34999999998, 216522.18000000002, 0.29788501638367587, 0.22912828714853356, 0.44420939454870423], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 156, 131, 83.97435897435898, 51471.3141025641, 141, 217564, 9043.0, 137170.1000000001, 176029.50000000003, 214171.93000000005, 0.3885807089605217, 0.2628631345884158, 0.6986104347620317], "isController": false}, {"data": ["getChildSemesterEvaluation", 124, 84, 67.74193548387096, 29332.16129032258, 3879, 124522, 9004.5, 97526.5, 116712.0, 124142.0, 0.3088956978800787, 0.1881457383603617, 0.47359984928879256], "isController": false}, {"data": ["getCentreManagementConfig", 132, 93, 70.45454545454545, 26514.280303030297, 1317, 120758, 9004.0, 88054.0, 112648.29999999999, 119369.35999999994, 0.3574542756405743, 0.2586259750445464, 0.5787687392696017], "isController": false}, {"data": ["updateClassResourceOrder", 126, 83, 65.87301587301587, 11371.841269841267, 141, 116004, 9003.0, 17401.19999999999, 34482.54999999999, 98809.05000000025, 0.38664301802492923, 0.2133842493218404, 0.36776396441043074], "isController": false}, {"data": ["getChildPortfolio", 118, 112, 94.91525423728814, 57714.415254237305, 9002, 223810, 9004.0, 189031.50000000006, 218298.09999999998, 223476.36000000002, 0.2939300944561795, 0.17891418206478418, 0.7110008241874576], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 132, 77, 58.333333333333336, 8361.431818181818, 150, 47259, 9002.5, 11416.5, 17929.099999999988, 44672.459999999905, 0.43292314958150757, 0.24239186760422954, 0.7901693032889041], "isController": false}, {"data": ["findAllCentreForSchool", 250, 243, 97.2, 66045.39600000001, 136, 224371, 9004.0, 215954.0, 222097.55, 224290.92, 0.6226991267267447, 16.209237725977513, 1.5308668843622812], "isController": false}, {"data": ["portfolioByID", 148, 115, 77.70270270270271, 41920.135135135155, 1308, 200260, 9328.0, 115778.59999999999, 121480.19999999995, 193579.82999999987, 0.36876555729694843, 0.28489105376003826, 1.2960812897575367], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 116, 87, 75.0, 50300.74137931034, 113, 221773, 9004.0, 125359.69999999998, 147173.19999999995, 217392.43999999994, 0.28914845779180315, 0.16066940672220312, 0.3848724101271755], "isController": false}, {"data": ["invoicesByFkChild", 109, 92, 84.40366972477064, 54710.28440366973, 9002, 209957, 9089.0, 142108.0, 190703.5, 209481.40000000002, 0.27158205266200247, 0.24613826749960135, 0.7892853405489446], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 128, 89, 69.53125, 8654.828125, 145, 62621, 9003.0, 9956.300000000005, 11667.699999999999, 49755.14999999972, 0.4095410260922427, 0.22686420655425618, 0.4475355548800972], "isController": false}, {"data": ["findAllFeeDraft", 140, 114, 81.42857142857143, 35987.55714285716, 117, 221689, 9005.0, 116194.3, 120923.84999999998, 219670.98, 0.34912544077086893, 0.18815659741098548, 0.5560777284153197], "isController": false}, {"data": ["getPastEvents", 124, 92, 74.19354838709677, 32634.838709677424, 140, 193874, 9004.0, 112248.5, 117723.0, 193367.25, 0.30896804672394207, 0.1615599989784121, 0.38922732448621605], "isController": false}, {"data": ["findAllConsolidatedRefund", 123, 98, 79.67479674796748, 47854.49593495934, 9002, 183484, 11276.0, 122719.6, 145005.8, 181446.64000000004, 0.3063908650684522, 0.22219370566050897, 0.5293021877989179], "isController": false}, {"data": ["getMyDownloadAlbum", 128, 91, 71.09375, 24134.023437499996, 323, 143301, 9004.0, 87940.40000000014, 118492.9, 137416.02999999988, 0.3188465725238948, 0.18094941938163692, 0.5034911208702518], "isController": false}, {"data": ["getRefundChildBalance", 119, 89, 74.78991596638656, 32500.226890756298, 150, 146819, 9004.0, 107550.0, 119328.0, 144710.19999999998, 0.29641881238183726, 0.18928760081975993, 0.4640228088360206], "isController": false}, {"data": ["findAllUploadedGiroFiles", 109, 80, 73.39449541284404, 40440.62385321101, 153, 216200, 9004.0, 116877.0, 148604.5, 214970.20000000007, 0.27151237374051884, 20.708775831818013, 0.38844299563462903], "isController": false}, {"data": ["findAllInvoice", 244, 219, 89.75409836065573, 52489.45491803275, 4681, 222154, 9005.5, 162202.5, 211195.0, 222145.0, 0.6079708971308259, 0.38167537717866623, 1.9538330859817858], "isController": false}, {"data": ["getAllArea", 121, 0, 0.0, 330.1239669421489, 50, 3712, 69.0, 943.8, 1865.0999999999983, 3670.6400000000003, 0.4036697247706422, 0.3168786488740617, 0.37962299311926606], "isController": false}, {"data": ["getChildChecklist", 128, 113, 88.28125, 60940.56250000001, 9002, 224289, 9028.5, 191265.3000000001, 216618.69999999998, 224088.32, 0.3227872680596955, 0.24686064558084056, 1.0629283866184502], "isController": false}, {"data": ["bankAccountInfoByIDChild", 129, 112, 86.82170542635659, 59234.240310077534, 124, 222044, 9004.0, 188493.0, 203269.5, 221925.19999999998, 0.3213375613717377, 0.21312307026207694, 0.7396412423370956], "isController": false}, {"data": ["findAllCreditDebitNotes", 115, 103, 89.56521739130434, 72957.33913043476, 142, 224346, 10409.0, 210658.80000000002, 218819.0, 224281.68, 0.28657650720553013, 0.19849365497808313, 0.6694248098004182], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 4091, 100.0, 75.02292316156245], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 5453, 4091, "502/Bad Gateway", 4091, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getUpcomingEvents", 118, 75, "502/Bad Gateway", 75, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllFeeGroup", 113, 74, "502/Bad Gateway", 74, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllChildActiveSubsidies", 130, 103, "502/Bad Gateway", 103, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getListDomainByLevel", 108, 77, "502/Bad Gateway", 77, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getPendingEvents", 129, 93, "502/Bad Gateway", 93, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getPortfolioTermByYear", 249, 171, "502/Bad Gateway", 171, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCurrentFeeTier", 131, 117, "502/Bad Gateway", 117, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllClassResources", 110, 67, "502/Bad Gateway", 67, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["listBulkInvoiceRequest", 138, 109, "502/Bad Gateway", 109, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCustomSubsidies", 112, 83, "502/Bad Gateway", 83, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 133, 108, "502/Bad Gateway", 108, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllChildDiscounts", 133, 105, "502/Bad Gateway", 105, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 109, 88, "502/Bad Gateway", 88, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getMyDownloadPortfolio", 130, 89, "502/Bad Gateway", 89, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getListDomain", 248, 174, "502/Bad Gateway", 174, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getLessonPlan", 113, 71, "502/Bad Gateway", 71, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAdvancePaymentReceipts", 112, 84, "502/Bad Gateway", 84, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllProgramBillingUpload", 118, 86, "502/Bad Gateway", 86, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 156, 131, "502/Bad Gateway", 131, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildSemesterEvaluation", 124, 84, "502/Bad Gateway", 84, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getCentreManagementConfig", 132, 93, "502/Bad Gateway", 93, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["updateClassResourceOrder", 126, 83, "502/Bad Gateway", 83, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildPortfolio", 118, 112, "502/Bad Gateway", 112, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 132, 77, "502/Bad Gateway", 77, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCentreForSchool", 250, 243, "502/Bad Gateway", 243, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["portfolioByID", 148, 115, "502/Bad Gateway", 115, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 116, 87, "502/Bad Gateway", 87, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["invoicesByFkChild", 109, 92, "502/Bad Gateway", 92, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 128, 89, "502/Bad Gateway", 89, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllFeeDraft", 140, 114, "502/Bad Gateway", 114, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getPastEvents", 124, 92, "502/Bad Gateway", 92, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllConsolidatedRefund", 123, 98, "502/Bad Gateway", 98, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getMyDownloadAlbum", 128, 91, "502/Bad Gateway", 91, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getRefundChildBalance", 119, 89, "502/Bad Gateway", 89, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllUploadedGiroFiles", 109, 80, "502/Bad Gateway", 80, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllInvoice", 244, 219, "502/Bad Gateway", 219, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getChildChecklist", 128, 113, "502/Bad Gateway", 113, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["bankAccountInfoByIDChild", 129, 112, "502/Bad Gateway", 112, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllCreditDebitNotes", 115, 103, "502/Bad Gateway", 103, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
